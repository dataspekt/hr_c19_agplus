# Antigenski pozitivni nalazi na SARS-CoV-2

Ne postoje javno dostupni podaci o broju COVID-19 dijagnoza postavljenih primjenom antigenskih testova, što umanjuje službeno objavljeni broj prepoznatih slučajeva. Ovaj repozitorij prikupljenih podataka iz različitih izvora je priručna alternativa.

## Izvori podataka

* Grad Zagreb, objave na [službenom Facebook profilu](https://web.facebook.com/zagreb.hr) i/ili [internetskim stranicama](https://www.zagreb.hr/gradske-vijesti/17), te medijski izvještaji. Podaci se odnose samo na testove provedene u domovima zdravlja i ne uključuju pozitivne nalaze iz domova za starije, bolnica ni privatnih laboratorija.

* Međimurska županija, [izvješća civilne zaštite](https://medjimurska-zupanija.hr/civilna-zastita/), odnosi se na podatke o testovima "koji se obavljaju u drugim zdravstvenim ustanovama i privatnim laboratorijima".

* Dubrovačko-neretvanska županija, [priopćenja stožera civilne zaštite](http://www.edubrovnik.org/category/koronavirus/).

* Vukovarsko-srijemska županija, [županijske internetske stranice](http://www.vusz.hr/info/novosti-najave-i-sluzbene-obavijesti). Antigensko testiranje se provodi od 23. studenoga ([ZZJZ VSŽ](https://www.zzjz-vsz.hr/vijesti/454-obavijest-o-uvodenju-brzih-antigenskih-testova-na-sars-cov-2-u-zzjz-vsz.html)), testiranje je dostupno u mikrobiološkim ispostavama ZJZ, vinkovačkoj i vukovarskoj bolnici i u domovima zdravlja ([novosti.hr](https://novosti.hr/brzi-antigenski-testovi-samo-preko-uputnice/)). Dio podataka je pribaljen ZPPI zahtjevom od ZJZ VSŽ, uz napomenu da se testiranje vrši i na lokacijama nad kojima oni nemaju pregled.

* Karlovačka županija, medijski izvještaji. Testiranje je najavljeno 24. studenog u pet županijskih domova zdravlja ([NovaTV](https://dnevnik.hr/vijesti/koronavirus/iz-minute-u-minutu-koronavirus-u-hrvatskoj-24-studenog-2020---629212.html)), ali započelo (izvan Karlovca?) tek 3. prosinca u domovima zdravlja u Ozlju, Ogulinu, Slunju, Dugoj Resi i Vojniću ([Radio Mrežnica](https://radio-mreznica.hr/brzo-antigensko-testiranje-kao-test-za-trudnocu-dvije-crtice-znace-pozitivno/)).

* Brodsko-posavska županija, [priopćenja stožera civilne zaštite](http://www.bpz.hr/novosti/stozer_civilne_zastite/default.aspx). Testiranje je počelo krajem studenog u OB "Dr. Josip Benčević" u Slavonskom Brodu, a od sredine prosinca u domovima zdravlja i kod liječnika koncesionara ([sbplus.hr](https://www.sbplus.hr/slavonski_brod/zivot/koronavirus/pazite_da_skupo_ne_platite_testove_koji_su_zapravo_besplatni.aspx)).

* Osječko-baranjska županija, [županijske internetske stranice](http://www.obz.hr/) i podaci pribavljeni ZPPI zahtjevom koji se odnose na KBC Osijek, OB Našice, ZJZ Osijek.

* U Krapinsko-zagorskoj županiji su (oko?) 18. prosinca "u domovima za starije i nemoćne krenula brza antigenska testiranja" uz 97 pozitivnih nalaza ([kzz.hr](http://www.kzz.hr/zupanijski-stozer-o-epidemioloskoj-situaciji-i-novim-mjerama)), ali nije jasno navedeno na koje dane se odnosi taj podatak (zabilježeni su kod 18. prosinca).

* Varaždinska županija, medijski izvještaji. Komercijalno dostupno u ZZJZ [od 26. studenog](https://www.zzjzzv.hr/?gid=2&aid=429). Zasad su dostupi podaci samo za testiranja zaposlenika Grupe Calzedonia za koja nije sigurno jesu li provođena u dijagnostičke svrhe bez potvrde PCR testom niti je do kraja poznata dnevna dinamika (17. prosinca je zabilježen kumulativni podatak).

* Zadarska županija, medijski izvještaji.

* Ličko-senjska županija, podaci pribavljeni ZPPI zahtjevom koji se odnose na testiranja u ZJZ Gospić.

## Županije i lokacije za koje podaci nisu dostupni

Antigenske testove koriste neki županijski zavodi za javno zdravstvo koji ne objavljuju podatke o broju pozitivnih nalaza, na primjer:

* Županijski NZJZ u Splitu [od 10. prosinca](http://hr.n1info.com/Vijesti/a585533/U-Splitu-pocelo-brzo-antigensko-testiranje-na-koronavirus.html). 

* U Virovitičko-podravskoj županiji u domu zdravlja testovi su stigli [krajem studenog](http://hr.n1info.com/Vijesti/a579997/U-viroviticki-Dom-zdravlja-stizu-brzi-antigenski-testovi.html).

Osim u okviru djelatnosti zavoda za javno zdravstvo, testiranja se provodi na još nekim mjestima, na primjer u ustanovama socijalne skrbi poput domova za starije (u [Zagrebu](http://hr.n1info.com/Vijesti/a584667/Zagreb-dobio-17.850-brzih-antigenskih-testova-za-60-ustanova-socijalne-skrbi.html)), u okviru službe hitne pomoći (npr. [u Zagrebu još od 9. studenoga](https://www.vecernji.hr/vijesti/od-danas-u-primjeni-brzi-testovi-ravnatelj-zagrebacke-hitne-ovo-ce-nam-dosta-olaksati-1444832), [u krapinsko-zagorskoj službi](https://vijesti.hrt.hr/681412/brzi-testovi-u-krapinsko-zagorskoj-karlovackoj-zupaniji-i-osijeku), na [hitnom bolničkom prijemu OB Koprivnica](https://kckzz.hr/u-ustanovama-socijalne-skrbi-na-podrucju-zupanije-pocinje-testiranje-brzim-antigenskim-testovima/)). Antigenska testiranja se u bolnicama provodi, prema dostupnim informacijama, kao probir simptomatskih pacijenata bez dodatne potvrde pozitivnih nalaza PCR testom, na primjer [u OB Zadar](https://radio.hrt.hr/radio-zadar/clanak/hocemo-li-i-u-zadru-moci-na-brzi-antigenski-test/251352/) su takav pristup isprobavali još krajem studenog, u [OB Karlovac](https://www.glasistre.hr/hrvatska/stozer-nema-uvjeta-za-popustanje-mjera-u-karlovackoj-zupaniji-685252), [OŽB Vinkovci](https://novosti.hr/brzi-antigenski-testovi-samo-preko-uputnice/), [zagrebačkoj Klinici za dječje bolesti](https://www.jutarnji.hr/vijesti/hrvatska/novi-pokusaj-suzbijanja-epidemije-sprema-se-masovno-testiranje-u-zagrebu-15031273), [OB Križevci](https://epodravina.hr/bolnica-je-krenula-s-brzim-antigenskim-testiranjem-stiglo-je-1250-testova/),... Antigenske testove u svrhu dijagnoze provode i liječnici primrne zdravstvene zaštite, na primjer [u Koprivničko-križevačkoj županiji](https://kckzz.hr/zupanijski-stozer-nadlezan-iskljucivo-za-propusnice-osoba-koje-trebaju-medicinsku-skrb-i-putuju-na-posao-gradani-se-mole-za-strpljenje-i-razumijevanje/),...

Prije nego je započeto testiranje u zagrebačkim domovima zdravlja, još početkom studenoga, antigenske testove su radili NZJZ Štampar, HZJZ i Klinika za infektivne bolesti ([Poslovni dnevnik](https://www.poslovni.hr/hrvatska/brzi-testovi-na-tri-lokacije-u-zagrebu-samo-za-one-sa-simptomima-4257367)).

Antigenske testove se provodi u nekim privatnim laboratorijima, a do službenih uputa objavljenih 25. studenog nije postojala obaveza prijave pozitivnih nalaza u platformu HZJZ. Npr. u Puli su [komercijalno dostupni u privatnom laboratoriju](https://www.glasistre.hr/istra/rezultati-brzog-testiranja-u-puli-u-dva-dana-otkrili-50-pozitivnih-osoba-oni-ne-ulaze-u-sluzbenu-statistiku-koliko-istra-zapravo-ima-oboljelih-od-covida-683044) od kraja studenog, itd.

## Napomene

Antigenske testove se koristi u dijagnostičke svrhe kada se pozitivne nalaze naknadno ne potvrđuje RT-PCR testom. Prema [uputama za primjenu antigenskih testova](https://www.hzjz.hr/wp-content/uploads/2020/03/Upotreba_brzih_antigenskih_testova_26_11-1.pdf) pozitivan nalaz se može smatrati pouzdanim ako je testirana simptomatska osoba i tada se temeljem njega može postaviti dijagnozu. Dijagnozu se ne postavlja kada je antigenski nalaz dobiven testiranjem osoba koje nemaju simptome. To je slučaj, na primjer, kada se radi istraživanje poput ispitivanja prevalencije ili opći probit u kolektivima. Tada je, načelno, nužno potvrditi pozitivan nalaz RT-PCR testom kako bi se osobu smatralo doista pozitivnom.

Postoje, međutim, slučajevi kada nije jasno koristi li se antigenske testove u svrhu postavljanja dijagnoze ili ne. Na primjer, ministar Vili Beroš je početkom prosinca [objavio](https://www.novilist.hr/novosti/hrvatska/beros-distribuirano-136-tisuca-brzih-antigenskih-testova-na-koronavirus/?meta_refresh=true) da je domovima za starije i bolnicama podijeljeno 150 tisuća antigenskih testova, a ravnatelj HZJZ Krunoslav Capak [pojasnio](https://www.youtube.com/watch?v=xYJSF21-nfc&t=1494) da njihova primjena neće biti dio dijagnostičkog postupka već "epidemiološkog projekta", odnosno pozitivne nalaze se neće smatrati dijagnozom, a osobe potvrđenim slučajevima. Istovremeno, ostalo je nejasno hoće li se u praksi pozitivan nalaz tih testova doista potvrđivati RT-PCR metodom. Ukoliko neće, takvi će nalazi u praksi predstavljati postavljanje dijagnoze uz izolaciju zaražene osobe, propisivanje samoizolacije njenim kontaktima i druge uobičajne mjere, ali pozitivne osobe neće biti evidentirane kao slučajevi. Prema uputama HZJZ, pri provođenju *screeninga* u kolektivima tumačenje rezultata testa ovisi o postotku pozitivnih nalaza u kolektivu na način da se u slučajevima kad je zatečena prevalencija visoka (npr. 20%) AG+ nalaze može smatrati doista pozitivnima, odnosno među njima se može očekivati tek manji broj lažno pozitivnih nalaza. 

## Upiti temeljem Zakona o pravu na pristup informacijama

Podatke o broju pozitivnih nalaza antigenskih testova koji su u posjedu [županijskih zavoda za javno zdravstvo](https://www.hzjz.hr/mreza-zavoda/) ili županijskih stožera Civilne zaštite može se pokušati dobiti zahtjevom upućenim sukladno Zakonu o pravu na pristup informacijama. Lokalni zavodi za javno zdravstvo ne raspolažu nužno cjelovitom informacijom, već samo dijelom koji se odnosi na testiranja koja su sami proveli, a županijski stožeri Civilne zaštite trebali bi imati sve podatke. Na nacionalnoj razini cjelovita informacija bi trebala biti u posjedu nacionalnog stožera Civilne zaštite (koji djeluje u okviru Ministarstva unutarnjih poslova), Hrvatskog zavoda za javno zdravstvo i Ministarstva zdravstva, a u dijelu koji se odnosi na testiranja u ustanovama socijalne skrbi (domova za starije i sl.) i Ministarstvo rada, mirovinskoga sustava, obitelji i socijalne politike.

### Primjer zahtjeva za pristup, odn. ponovnu upotrebu informacija

```
To: <e-mail lokalnog ZJZ, županijskog stožera, odn. županije>
Subject: Zahtjev za ponovnu upotrebu informacija
Poštovani,
Molim da u svrhu ponovne upotrebe <tijelo javne vlasti> dostavi informaciju 
o broju pozitivnih nalaza i provedenih testiranja, oboje po pojedinim danima 
od početka testiranja i lokacijama uzimanja uzoraka, koja se odnosi na 
testiranje brzim antigenskim testovima na SARS-CoV-2 na području 
<županije> koje se provodi u dijagnostičke svrhe, odnosno u svim 
slučajevima gdje se pozitivne nalaze testova naknadno ne potvrđuje 
RT-PCR testom. Svrha upotrebe zatražene informacije je nekomercijalna, 
a predloženi način dostave informacije elektroničkim putem na adresu 
<vaša e-mail adresa>. Predlažem i da zatraženu informaciju redovito 
objavljujete u dnevnim priopćenjima ili proaktivno učinite dostupnom 
u obliku za ponovnu upotrebu na vašim internetskim stranicama.
S poštovanjem,
<ime i prezime>
<adresa, grad>
```

### ZJZ Šibensko-kninske županije

* 2020-12-13: Ponovljen zahtjev zbog pogrešno navedene adrese službenika za informiranje na internetskim stranicama.

### ZJZ Krapinsko-zagorske županije

* 2020-12-14: *"ovim Vas putem obavještavamo da naš Zavod nije do sada provodio testiranje brzim antigenskim testovima na SARS-CoV-2 na području Krapinsko-zagorske županije."*

### NZJZ "Dr. Andrija Štampar", Zagreb 

* 2020-12-14: Traže da se zahtjev ponovi na obrascu, netočno napominjući da *"možemo proslijediti Vaš upit stručnim službama i ne moraju Vam odgovoriti"*.

* 2020-12-18: *"Obavještavamo Vas da smo Vaš zahtjev za ponovnu upotrebu informacija, ustupili Gradskom uredu za zdravstvo Grada Zagreba na daljnje postupanje sukladno članku 21. stavak 1. Zakona o pravu na pristup informacijama."*

### NZJZ Splitsko-dalmatiske županije

* 2020-12-17: Ponovljen zahtjev zbog pogrešno navedene adrese službenika za informiranje na internetskim stranicama.

### ZJZ Zagrebačke županije

* 2020-12-17: Ponovljen zahtjev jer je službenica za informiranje u izolaciji.

### NZJZ Primorsko-goranske županije

* 2020-12-18: *"Brze antigenske testove NZZJZ PGŽ nije do sada provodio ni u drive in-u niti u Ispostavama."*

### ZJZ Ličko-senjske županije

* 2020-12-21: Pozitivan odgovor na zahtjev za razdoblje od 18. do 21. prosinca.

### Osječko-baranjska županija

* 2020-12-21: Pozitivan odgovor na zahtjev za razdoblje od 23. studenog do 14. prosinca na lokacijama KBC Osijek, OB Našice i ZJZ Osijek.

### Požeško-slavonska županija

* 2020-12-21: *"prema izjavi ravnatelja ZJZ Požega  obavještavam Vas da Zavod za javno zdravstvo Požega ne vrši testiranje na SARS-Cov-2 putem RT-PCR metode testiranja. Testiranje RT-PCR metodom u Požeško-slavonskoj županiji vrši samo Opća županijska bolnica Požega te ravnatelj predlaže da navedene podatke zatražite od OŽB Požega."*

### ZJZ Sisačko-moslavačke županije

* 2020-12-23: *"Zavod za javno zdravstvo Sisadko-moslavačke županije još ne provodi brzo antigensko testiranje na SARS-CoV-2, u postupku smo ispunjavanja uvjeta za uspostavu i provođenje antigenskih testova, a traženu informaciju nema kao ni saznanja gdje se informacija koju ovlaštenik traži, nalazi."*

### ZJZ Vukovarsko-srijemske županije

* 2020-12-24: *"Dostavljamo Vam u privitku tablicu sa brojem obavljenih testiranja brzim antigenskim testovima na SARS-CoV-2 i brojem pozitivnih nalaza po danima i lokacijama uzimanja uzoraka, koji su obavljeni u Zavodu za javno zdravstvo Vukovarsko-srijemske županije. Ne možemo Vam dostaviti podatke za područje cijele Vukovarsko-srijemske županije jer se testiranje brzim antigenskim testovima obavlja i u drugim zdravstvenim ustanovama, na različitim lokacijama i mi ne raspolažemo relevantnim i potpunim podacima o tome koliko je ukupno napravljeno brzih antigenskih testova u drugim zdravstvenim ustanovama."*
